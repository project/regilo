<?php 

/**
 * @file
 * Useful functions
 *
 */

define('LOGIN_ERROR', 801);

function _get_users_number() {
  $sql = 'SELECT COUNT(*) FROM {users}';
  $total = db_result(db_query($sql));
  //remove anonymous user from count
  return $total - 1;
}
